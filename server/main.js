import { Meteor } from 'meteor/meteor';
import {TelliesCollection} from '../api/Tellies';

Meteor.startup(() => {
  if(!TelliesCollection.find({}).count()) {
    console.log('here');
    const tellies = [
      {
        name: 'ТВ-1',
        room: 'Внутренняя',
        price: 500
      },
      {
        name: 'V.I.P.-1',
        room: 'Внешняя',
        price: 800
      }
    ];

    tellies.map(item => {
      TelliesCollection.insert(item);
    });
  }
});
