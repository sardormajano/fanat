import {Mongo} from 'meteor/mongo';

export const TelliesCollection = new Mongo.Collection('tellies');

if(Meteor.isServer) {
  Meteor.publish('Tellies.All', () => {
    return TelliesCollection.find();
  });

  Meteor.methods({
    'tellies.add'(data) {
      data.createdBy = Meteor.userId;
      const createdAt = new Date();
      data.createdAt = Date.parse(createdAt);

      TelliesCollection.insert(data);
    },
    'tellies.remove'(_id) {
      TelliesCollection.remove({_id})
    },
    'tellies.edit'(_id, data) {
      data.updatedBy = Meteor.userId;
      const updatedAt = new Date();
      data.updatedAt = Date.parse(updatedAt);

      TelliesCollection.update({_id}, {$set: data});
    }
  });
}
