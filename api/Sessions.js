import {Mongo} from 'meteor/mongo';

export const SessionsCollection = new Mongo.Collection('sessions');

if(Meteor.isServer) {
  Meteor.publish('Sessions.All', () => {
    return SessionsCollection.find({});
  });

  Meteor.publish('Sessions.Open', () => {
    return SessionsCollection.find({status: 1});
  });

  Meteor.methods({
    'sessions.add'(data) {
      data.createdBy = Meteor.userId;
      const createdAt = new Date();
      data.createdAt = Date.parse(createdAt);

      SessionsCollection.insert(data);
    },
    'sessions.remove'(_id) {
      SessionsCollection.remove({_id})
    },
    'sessions.edit'(_id, data) {
      data.updatedBy = Meteor.userId;
      const updatedAt = new Date();
      data.updatedAt = Date.parse(updatedAt);

      SessionsCollection.update({_id}, {$set: data});
    }
  });
}
