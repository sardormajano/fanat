import React, {Component} from 'react';

import AppBar from 'material-ui/AppBar';
import NavigationClose from 'material-ui/svg-icons/navigation/close';
import FlatButton from 'material-ui/FlatButton';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';

import {browserHistory} from 'react-router';

function handleTouchTap() {
  alert('onTouchTap triggered on the title component');
}

const styles = {
  title: {
    cursor: 'pointer',
  },
};

/**
 * This example uses an [IconButton](/#/components/icon-button) on the left, has a clickable `title`
 * through the `onTouchTap` property, and a [FlatButton](/#/components/flat-button) on the right.
 */
export default class NavBar extends Component{
  render() {
    const navButtons = (
      <IconMenu
        iconButtonElement={
          <IconButton><MoreVertIcon /></IconButton>
        }
        targetOrigin={{horizontal: 'right', vertical: 'top'}}
        anchorOrigin={{horizontal: 'right', vertical: 'top'}}
      >
        <MenuItem
          onTouchTap={e => browserHistory.push('/')}
          primaryText="Телевизоры"
        />
        <MenuItem
          onTouchTap={e => browserHistory.push('/report')}
          primaryText="Отчет"
        />
        <MenuItem
          onTouchTap={e => browserHistory.push('/tvs-crud')}
          primaryText="ТВ CRUD"
        />
        <MenuItem
          onTouchTap={e => browserHistory.push('/users-crud')}
          primaryText="Пользователи CRUD"
        />
      </IconMenu>
    );

    return (
      <AppBar
        title={<span style={styles.title}>PS4 Фанат</span>}
        onTitleTouchTap={handleTouchTap}
        iconElementRight={navButtons}
        showMenuIconButton={false}
      />
    );
  }
}
