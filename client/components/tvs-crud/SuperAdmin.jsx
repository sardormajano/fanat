import React, {Component} from 'react';

import NavBar from '../stateless/NavBar';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

export default class SuperAdmin extends Component {
  render() {
    return (
      <MuiThemeProvider>
        <div>
          <NavBar />
        </div>
      </MuiThemeProvider>
    );
  }
}
