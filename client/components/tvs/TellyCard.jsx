import React, {Component} from 'react';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';
import Divider from 'material-ui/Divider';
import {
  blue300,
  red600,
} from 'material-ui/styles/colors';

import moment from 'moment';

import Avatar from 'material-ui/Avatar';

const styles = {
  root: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  card: {
    width: '28%',
    margin: '2%',
    float: 'left'
  },
  activeTV: undefined
};

const beautifySeconds = totalSecs => {
  let totalmins = parseInt(totalSecs / 60),
        secs = totalSecs % 60,
        mins = totalmins % 60,
        hours = parseInt(totalmins / 60);

  secs = Math.abs(secs) < 10 ? `${secs < 0 ? '-' : ''}0${Math.abs(secs)}` : `${secs}`;
  mins = Math.abs(mins) < 10 ? `${mins < 0 ? '-' : ''}0${Math.abs(mins)}` : `${mins}`;
  hours = Math.abs(hours) < 10 ? `${hours < 0 ? '-' : ''}0${Math.abs(hours)}` : `${hours}`;

  return `${hours}:${mins}:${secs}`;
}

const CustomDivider = props => {
  return (
    <div>
      <br />
      <Divider /><br />
    </div>
  );
}

export default class TellyCard extends Component {
  renderMainButtons() {
    const {session, context} = this.props;

    if(session) {
      return (
        [
          <FlatButton
            key={0}
            label="Добавить"
            onClick={context.updateSession.bind(context)}
          />,
          <FlatButton
            key={1}
            label="Закрыть"
            onClick={context.endSession.bind(context)}
          />
        ]
      );
    }
    else {
      return (
        <FlatButton
          label="Открыть"
          onClick={context.startSession.bind(context)}
        />
      );
    }
  }

  renderTimers() {
    if(!this.props.session) {
      return [
        <TextField
          key={0}
          floatingLabelStyle={{textAlign: 'center'}}
          style={{width: '40%', color: 'black'}}
          floatingLabelText="Прошло"
          disabled={true}
        />,
        <TextField
          key={1}
          floatingLabelStyle={{textAlign: 'center'}}
          style={{width: '40%', marginLeft: '10%'}}
          floatingLabelText="Осталось"
          disabled={true}
        />
      ];
    }

    const {session, context} = this.props,
          start = session.start,
          end = session.end,
          now = moment().unix(),
          left = end ? beautifySeconds(end - now) : 'открытое время',
          passed = beautifySeconds(now - start);

    setTimeout(this.forceUpdate.bind(this), 1000);

    return [
      <TextField
        key={2}
        floatingLabelStyle={{textAlign: 'center'}}
        style={{width: '40%', color: 'black'}}
        floatingLabelText="Прошло"
        value={passed}
      />,
      <TextField
        key={3}
        floatingLabelStyle={{textAlign: 'center'}}
        style={{width: '40%', marginLeft: '10%'}}
        floatingLabelText="Осталось"
        value={left}
      />
    ];
  }

  render() {
    const {telly, session, context} = this.props,
          tellyAvatar = (
            <Avatar
              color='white'
              backgroundColor={session ? red600 : blue300}
              size={30}
            >
              {session ? 'З' : 'С'}
            </Avatar>
          ),
          {addTime} = context,
          hours = parseInt(context.state.minutes / 60),
          minutes = context.state.minutes % 60;

    return (
      <Card
        style={styles.card}
        expanded={telly._id === context.state.activeTelly}
        onExpandChange={(exp) => context.setState({activeTelly: exp ? telly._id : undefined})}
      >
        <CardHeader
          title={telly.name}
          subtitle={telly.room}
          actAsExpander={true}
          showExpandableButton={true}
          avatar={tellyAvatar}
        />
        <CardText style={{textAlign: 'center'}}>
          {this.renderTimers()}
          {' '}
        </CardText>
        <CardText
          expandable={true}
          style={{textAlign: 'center'}}
        >
          <Divider />
          <TextField
            floatingLabelStyle={{textAlign: 'center'}}
            style={{width: '30%'}}
            floatingLabelText="Часы"
            value={hours}
          />{' '}
          <TextField
            floatingLabelStyle={{textAlign: 'center'}}
            style={{width: '30%'}}
            floatingLabelText="Минуты"
            value={minutes}
          />{' '}
          <TextField
            floatingLabelStyle={{textAlign: 'center'}}
            style={{width: '30%'}}
            floatingLabelText="К оплате (тг.)"
            value={context.state.price}
          /> {' '}
          <FlatButton
            label="5 мин"
            primary={true}
            onClick={addTime.bind(context, 0, 5, parseInt(telly.price / 12), '5 мин')}
          />
          <FlatButton
            label="10 мин"
            primary={true}
            onClick={addTime.bind(context, 0, 10, parseInt(telly.price / 6), '10 мин')}
          />
          <FlatButton
            label="30 мин"
            primary={true}
            onClick={addTime.bind(context, 0, 30, parseInt(telly.price / 2), '30 мин')}
          />
          <FlatButton
            label="1 час"
            primary={true}
            onClick={addTime.bind(context, 1, 0, telly.price, '1 час')}
          />
          <FlatButton
            label="3 + 1"
            primary={true}
            onClick={addTime.bind(context, 4, 0, telly.price * 3, '3+1')}
          />
          <FlatButton
            label="Ночь"
            primary={true}
            onClick={addTime.bind(context, 7, 0, telly.price * 6, 'Ночь')}
          />
          <CustomDivider />
          <FlatButton
            label="Сброс"
            secondary={true}
            onClick={
              () => context.setState({
                minutes: 0,
                price: 0,
                history: []
              })
            }
          />
          <FlatButton
            label="История"
            default={true}
            onClick={() => context.setState({snackbarOpen: true})}
          />
          <CustomDivider />
            {this.renderMainButtons()}
          <CustomDivider />
          <TextField
            floatingLabelStyle={{textAlign: 'center'}}
            style={{width: '100%'}}
            floatingLabelText="К оплате - сумма (тг.)"
            value={session ? session.price : 0}
          /> {' '}
          <TextField
            floatingLabelStyle={{textAlign: 'center'}}
            style={{width: '30%', color: 'black'}}
            floatingLabelText="Оплата (тг.)"
            value={context.state.paid}
            onChange={(e, paid) => context.setState({paid})}
          />{' '}
          <FlatButton
            label="Оплатить"
            default={true}
            onClick={context.conductPayment.bind(context)}
          />
          <TextField
            value={context.state.comment}
            onChange={(e, comment) => context.setState({comment})}
            floatingLabelText="Комментарий"
            style={{width: '95%', color: 'black'}}
            multiLine={true}
            rows={2}
            rowsMax={4}
          />
        </CardText>
      </Card>
    );
  }
}
