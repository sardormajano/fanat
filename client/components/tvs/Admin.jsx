import React, {Component} from 'react';
import {createContainer} from 'meteor/react-meteor-data';

//MATERIAL UI COMPONENTS
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import Toggle from 'material-ui/Toggle';
import Dialog from 'material-ui/Dialog';
import TextField from 'material-ui/TextField';
import Divider from 'material-ui/Divider';
import {List, ListItem} from 'material-ui/List';
import Subheader from 'material-ui/Subheader';
import Snackbar from 'material-ui/Snackbar';

//CUSTOM COMPONENTS
import TellyCard from './TellyCard';
import NavBar from '../stateless/NavBar';

const styles = {
  root: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  card: {
    width: '28%',
    margin: '2%',
    float: 'left'
  },
  activeTV: undefined
};

export default class Admin extends Component {
  renderTellies() {
    const {tellies, sessions} = this.props.context.props;

    return tellies.map((telly, index) => {
      const session = sessions.filter(s => s.telly === telly._id)[0];

      return (
        <TellyCard
          key={index}
          context={this.props.context}
          telly={telly}
          session={session}
        />
      );
    });
  }

  renderSnackbar() {
    const {context} = this.props;

    return (
      <Snackbar
        open={context.state.snackbarOpen}
        message={context.state.history.join(';')}
        autoHideDuration={4000}
        onRequestClose={() => context.setState({snackbarOpen: false})}
      />
    );
  }

  render() {

    return (
      <div>
        <MuiThemeProvider>
          <NavBar />
        </MuiThemeProvider>
        <MuiThemeProvider>
          <div>
            {this.renderTellies()}
            {this.renderSnackbar()}
          </div>
        </MuiThemeProvider>
      </div>
    );
  }
}
