import React, {Component} from 'react';

//MATERIAL UI COMPONENTS
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn, TableFooter} from 'material-ui/Table';

//LIBS
import moment from 'moment';

//CUSTOM LIBS
import {idToName} from '/client/customLib/collectionsRelated';
import NavBar from '../stateless/NavBar';

export default class SuperAdmin extends Component {
  renderRows() {
    const {context} = this.props,
          {sessions, tellies} = context.props;

    return sessions.map((session, index) => {
      return (
        <TableRow key={index}>
          <TableRowColumn>{idToName(session.telly, tellies)}</TableRowColumn>
          <TableRowColumn>{moment.unix(session.start).calendar()}</TableRowColumn>
          <TableRowColumn>{moment.unix(session.end).calendar()}</TableRowColumn>
          <TableRowColumn>{session.price}</TableRowColumn>
          <TableRowColumn>{session.paid}</TableRowColumn>
          <TableRowColumn>{session.comment ? session.comment.join(';') : ''}</TableRowColumn>
        </TableRow>
      );
    });
  }

  render() {
    return (
      <div>
        <MuiThemeProvider>
          <NavBar />
        </MuiThemeProvider>
        <MuiThemeProvider>
          <div>
            <Table
              fixedHeader={true}
              fixedFooter={true}
              height='400px'
            >
              <TableHeader>
                <TableRow>
                  <TableHeaderColumn>ТВ</TableHeaderColumn>
                  <TableHeaderColumn>Начало</TableHeaderColumn>
                  <TableHeaderColumn>Конец</TableHeaderColumn>
                  <TableHeaderColumn>Сумма</TableHeaderColumn>
                  <TableHeaderColumn>Оплатил</TableHeaderColumn>
                  <TableHeaderColumn>Комментарий</TableHeaderColumn>
                </TableRow>
              </TableHeader>
              <TableBody>
                {this.renderRows()}
              </TableBody>
              <TableFooter>
                <TableRow>
                  <TableRowColumn>ID</TableRowColumn>
                  <TableRowColumn>Name</TableRowColumn>
                  <TableRowColumn>Status</TableRowColumn>
                </TableRow>
                <TableRow>
                  <TableRowColumn colSpan="3" style={{textAlign: 'center'}}>
                    Super Footer
                  </TableRowColumn>
                </TableRow>
              </TableFooter>
            </Table>
          </div>
        </MuiThemeProvider>
      </div>
    );
  }
}
