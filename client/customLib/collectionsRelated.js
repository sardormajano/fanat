export const idToName = (_id, collectionArray) => {
  const doc = collectionArray.filter(item => item._id === _id)[0];

  return doc ? doc.name : 'не найдено'
}
