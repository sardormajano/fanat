import React from 'react';

import App from './components/App';
import TVsAdmin from './containers/tvs/AdminContainer';
import ReportSuperAdmin from './containers/report/SuperAdminContainer';
import TVsCrudSuperAdmin from './containers/tvs-crud/SuperAdminContainer';

import {Router, Route, IndexRoute, browserHistory} from 'react-router';

export const renderRoutes = () => (
  <Router history={browserHistory}>
    <Route path="/" component={App}>
      <IndexRoute component={TVsAdmin} />
      <Route path="report" component={ReportSuperAdmin} />
      <Route path="tvs-crud" component={TVsCrudSuperAdmin} />
    </Route>
  </Router>
);
