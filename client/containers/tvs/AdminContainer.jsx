import React, {Component} from 'react';

import {createContainer} from 'meteor/react-meteor-data';

//COLLECTIONS
import {TelliesCollection} from '/api/Tellies';
import {SessionsCollection} from '/api/Sessions';

//CUSTOM LIBS
import {multipleSubscribe} from '/client/customLib/meteorRelated';

//ADDITIONAL LIBS
import moment from 'moment';

//CUSTOM COMPONENTS
import Admin from '../../components/tvs/Admin';

class AdminContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      activeTelly: undefined,
      minutes: 0,
      price: 0,
      history: [],
      paid: 0,
      comment: '',
      snackbarOpen: false,
    }
  }

  componentWillMount() {
    multipleSubscribe(['Sessions.Open', 'Tellies.All']);
  }

  addTime(hours, minutes, price, history) {
    this.setState({
      minutes: this.state.minutes + minutes + (hours * 60),
      price: this.state.price + price,
      history: [...this.state.history, history]
    });
  }

  startSession() {
    const start = moment(),
          end = this.state.minutes ? moment().add(this.state.minutes, 'minutes') : undefined;

    const data = {
      telly: this.state.activeTelly,
      status: 1,
      start: start.unix(),
      end: end ? end.unix() : undefined,
      history: this.state.history,
      price: this.state.price,
      paid: parseInt(this.state.paid)
    }

    Meteor.call('sessions.add', data, err => {
      if(err) {
        console.log(err);
      }
      else {
        console.log('add success');
        this.setState({
          minutes: 0,
          price: 0,
          history: [],
        });
      }
    });
  }

  updateSession() {
    if(!this.state.minutes) {
      alert('нечего добавлять');
      return;
    }

    const currentSession = SessionsCollection.findOne({
            telly: this.state.activeTelly,
            status: 1
          }),
          end = moment.unix(currentSession.end).add(this.state.minutes, 'minutes');

    const data = {
      telly: this.state.activeTelly,
      end: end.unix(),
      history: [...currentSession.history, ...this.state.history],
      price: currentSession.price + this.state.price
    }

    Meteor.call('sessions.edit', currentSession._id, data, err => {
      if(err) {
        console.log(err);
      }
      else {
        console.log('edit success');
        this.setState({
          minutes: 0,
          price: 0,
          history: [],
        });
      }
    });
  }

  conductPayment() {
    const currentSession = SessionsCollection.findOne({
            telly: this.state.activeTelly,
            status: 1
          }),
          currentComment = currentSession.comment || [],
          currentPaid = currentSession.paid || 0;

    const data = {
      paid: parseInt(this.state.paid) + parseInt(currentPaid),
      comment: [...currentComment, this.state.comment]
    }

    Meteor.call('sessions.edit', currentSession._id, data, err => {
      if(err) {
        console.log(err);
      }
      else {
        console.log('payment success');
        this.setState({
          paid: 0,
          comment: ''
        });
      }
    });
  }

  endSession() {
    if(!confirm('Вы точно хотите закрыть данную сессию?'))
      return ;

    const currentSession = SessionsCollection.findOne({
            telly: this.state.activeTelly,
            status: {$ne: 0}
          }),
          closedAt = moment().unix();

    Meteor.call('sessions.edit', currentSession._id, {
      status: 0,
      closedAt
    },
    err => {
      if(err) {
        console.log(err);
      }
      else {
        console.log('end success');
      }
    });
  }

  render() {
    return (
      <Admin context={this}/>
    );
  }
}

export default createContainer(() => {
  return {
    user: Meteor.user(),
    sessions: SessionsCollection.find().fetch(),
    tellies: TelliesCollection.find().fetch()
  }
}, AdminContainer);
