import React, {Component} from 'react';
import {createContainer} from 'meteor/react-meteor-data';
import SuperAdmin from '../../components/report/SuperAdmin';

//CUSTOM LIBS
import {multipleSubscribe} from '/client/customLib/meteorRelated';

//COLLECTIONS
import {TelliesCollection} from '/api/Tellies';
import {SessionsCollection} from '/api/Sessions';

class SuperAdminContainer extends Component {
  componentWillMount() {
    multipleSubscribe(['Sessions.All', 'Tellies.All']);
  }

  render() {
    return (
      <SuperAdmin context={this}/>
    );
  }
}

export default createContainer(() => {
  return {
    tellies: TelliesCollection.find().fetch(),
    sessions: SessionsCollection.find().fetch()
  }
}, SuperAdminContainer);
