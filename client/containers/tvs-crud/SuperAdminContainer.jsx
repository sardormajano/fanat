import React, {Component} from 'react';
import {createContainer} from 'meteor/react-meteor-data';

import SuperAdmin from '../../components/tvs-crud/SuperAdmin';

class SuperAdminContainer extends Component {
  render() {
    return (
      <SuperAdmin />
    );
  }
}

export default createContainer(() => {
  return {};
}, SuperAdminContainer);
